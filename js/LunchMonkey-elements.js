"use strict";

/*
 * LunchMonkey-elements.js
 *
 * Each function creates or is part of creating specific elements at the UI.
 * This is a collection of reusable "components".
 */

function banner(user) {
  return `
    <div class="container-fluid p-0">
      <div class="row align-items-center gap-2">
        <div class="col-2 p-0 monkey-head-container">
          <img class="monkey-head" src="assets/img/Monkey Head.png" alt="LunchMonkey Head Logo"/>
        </div>
        <div class="col p-0 user-details text-end">
          ${userDetails(user)}
        </div>
        <div class="col-2 p-0 avatar-container">
          <img class="avatar" src="${user ? user.avatar : LUNCHMONKEY_DEFAULT_AVATAR}" alt="User Avatar"/>
        </div>
      </div>
    </div>
  `;
}

function disclaimer() {
  return `
    <div class='disclaimer text-center text-wrap fw-light pb-4'>
      <small>
        <div class="d-block">This is LunchMonkey, a small app to for educational purpose.</div>
        <div class="d-block">
          © Copyright 2023, Oliver Maurhart, HTL Villach, 
          <a class="email-address" href="mailto:oliver.maurhart@htl-villach.at">oliver.maurhart@htl-villach.at</a>.
        </div> 
      </small>
    </div>`;
}

function loginSubTitle() {
  return `
    <div class="container login-banner p-0 sub-title">
      <div class="row align-items-center justify-content-center">
        <div class="col-2 p-0 pe-3 monkey-head-container">
          <img class="monkey-head" src="assets/img/Monkey Head.png" alt="LunchMonkey Head Logo"/>
        </div>
        <div class="col-6 p-0 slogan">Save. Meet. Share. Enjoy.</div>
      </div>
    </div>
  `;
}

function loginTitle() {
  return `<h1 class='login-banner title text-center'>LunchMonkey</h1>`;
}

function memberList(wallet, currentUserName) {
  
  const members = [];
  if (wallet) {
    
    const sumsPerUser = wallet.sumPerUser();
    let averagePerUser = 0.0;
    for (const amount of sumsPerUser.values()) {
      averagePerUser += amount;
    }
    averagePerUser /= wallet.usernames.length;
    
    for (const i in wallet.usernames) {
      const user = LunchMonkey.getUserByName(wallet.usernames[i]);
      const userBalance = sumsPerUser.get(user.username) - averagePerUser;
      
      let actionCell;
      if (user.username === wallet.adminname) {
        actionCell = "Wallet Admin";
      }
      else if (wallet.disabledUsernames.includes(user.username)) {
        actionCell = "removed";
      } else if (wallet.adminname === currentUserName) {
        actionCell = `
          <button class="btn btn-secondary d-flex flex-row ms-auto" onclick="disableUserInWallet('${wallet.name}', '${user.username}')">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 384 512"><!--! Font Awesome Pro 6.4.0 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M342.6 150.6c12.5-12.5 12.5-32.8 0-45.3s-32.8-12.5-45.3 0L192 210.7 86.6 105.4c-12.5-12.5-32.8-12.5-45.3 0s-12.5 32.8 0 45.3L146.7 256 41.4 361.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0L192 301.3 297.4 406.6c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L237.3 256 342.6 150.6z"/></svg>
            Disable
          </button>`;
      } else {
        actionCell = "";
      }
      
      members.push(`
        <div class="row member align-items-center my-2">
          <div class="col d-flex flex-row gap-2">
            <img class="avatar" src="${user ? user.avatar : LUNCHMONKEY_DEFAULT_AVATAR}" alt="${user.username}"/>
            <div class="my-auto userCell">${user.username}</div>
          </div>
          <div class="col totalCell">€ ${sumsPerUser.get(user.username).toFixed(2)}</div>
          <div class="col balanceCell ${ userBalance >= 0 ? 'positive' : 'negative' }">€ ${userBalance.toFixed(2)}</div>
          <div class="col text-end">${ actionCell }</div>
        </div>
      `);
    }
  }
  
  return members.join("");
}

function recentWalletButton(user, cssClass) {
  let wallet = user ? LunchMonkey.getWallet(user.recentWalletname) : undefined;
  return walletButton(wallet, cssClass);
}

function userDetails(user) {
  return `
    <div class="d-flex flex-column gap-0 user-details">
      <div class="user-name">${user ? user.username : ''}</div>
      <div class="user-email">${user ? user.email : ''}</div>
    </div>
  `;
}

function walletAvatars(wallet, clickedCallback, callbackMouseOver, callbackMouseLeave) {
  
  if (!wallet) {
    return "";
  }
  
  const avatars = [];

  let i = 0;
  for (const username of wallet.usernames) {
    
    if (wallet.disabledUsernames.includes(username)) {
      continue;
    }
    const user = LunchMonkey.getUserByName(username);
    const clickDefinition = clickedCallback ? `onclick="${clickedCallback.name}('${user.username}', this);"` : "";
    const mouseMoveDefinition = callbackMouseOver ? `onmouseover="${callbackMouseOver.name}('${user.username}', this);"` : "";
    const mouseLeaveDefinition = callbackMouseLeave ? `onmouseleave="${callbackMouseLeave.name}('${user.username}', this);"` : "";
    avatars.push(`
      <img class="avatar position-absolute"
          style="left: ${i * 3}em;"
          src="${user ? user.avatar : LUNCHMONKEY_DEFAULT_AVATAR}" alt="${user.username}"
          ${clickDefinition} ${mouseMoveDefinition} ${mouseLeaveDefinition}
      />
    `);
    i = i + 1;
  }

  return avatars.join("");
}


function walletButton(wallet, cssClass) {

  const disabled = !wallet ? "disabled" : "";
  const balance = wallet ? "€ "+ wallet.balance().toFixed(2) : "";

  let buttonCSSStyle = "btn-outline-secondary";
  if (!!wallet) {
    buttonCSSStyle = cssClass ? cssClass : "btn-secondary";
  }

  return `
    <button type="button" 
        class="d-flex flex-row justify-content-between wallet-button btn ${buttonCSSStyle}" 
        ` + disabled + `
        onclick="clickedWalletButton('${wallet ? wallet.name : undefined}')">
      <div class="wallet-name">${wallet ? wallet.name : "No recent wallet found."}</div>
      <div class="wallet-balance">${balance}</div>
    </button>
  `;
}

function walletList(user) {

  if (!user) {
    return "";
  }

  let buttonList = "";
  const walletsOfUser = LunchMonkey.getWalletNamesByUserName(user.username);
  for (let i in walletsOfUser) {
    const wallet = LunchMonkey.getWallet(walletsOfUser[i]);
    buttonList += walletButton(wallet);
  }

  return `${buttonList}`;
}

console.debug("LunchMonkey-elements.js loaded.");
