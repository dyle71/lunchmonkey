"use strict";

/*
 * LunchMonkey-data.js
 *
 * Data model of LunchMonkey.
 */

const LUNCHMONKEY_DEFAULT_AVATAR = "assets/icons/user-solid.svg";

/**
 * The LunchMonkey application itself: the root of all application data processing.
 */
class Application {

  #currentUsername
  #currentWalletname
  #users
  #wallets

  constructor() {
    this.#users = new Map();
    this.#wallets = new Map();
  }

  addUser(user) {
    if (!user) {
      throw "No user to add to application.";
    }
    if (this.#users.has(user.username)) {
      throw "User exists already.";
    }
    this.#users.set(user.username, user);
  }

  addWallet(wallet) {

    if (!wallet) {
      throw "No wallet to add to application.";
    }

    if (this.#wallets.has(wallet.name)) {
      throw "Wallet exists already.";
    }

    for (const i in wallet.users) {
      const user = wallet.users[i];
      if (!this.#users.has(user)) {
        throw `Unknown user in wallet: ${user}.`;
      }
    }

    this.#wallets.set(wallet.name, wallet);
  }

  get currentUsername() {
    return this.#currentUsername;
  }

  get currentWalletname() {
    return this.#currentWalletname;
  }

  get users() {
    // This creates a deep copy of the users map.
    // We do not want that the callee mingles with our core data.
    return new Map([...this.#users]);
  }

  get wallets() {
    // This creates a deep copy of the wallets map.
    // We do not want that the callee mingles with our core data.
    return new Map([...this.#wallets]);
  }

  getUserByEmail(email) {
    for (const i in this.#users) {
      const user = this.#users[i];
      if (user.email === email) {
        return user;
      }
    }
    return null;
  }

  getUserByName(username) {
    return this.#users.get(username);
  }

  getWallet(walletname) {
    if (!walletname) {
      return null;
    }
    return this.#wallets.get(walletname);
  }

  getWalletNamesByUserName(username) {
    return [...this.#wallets]
        .filter(([_, wallet]) => wallet.hasUser(username))
        .map(([_, wallet]) => wallet.name);
  }

  login(userNameOrEmail, password) {

    if (this.#currentUsername) {
      throw "There is a user logged in. Log out first.";
    }

    let user = this.getUserByName(userNameOrEmail);
    if (!user) {
      user = this.getUserByEmail(userNameOrEmail);
    }
    if (!user) {
      throw "Unknown user or password.";
    }

    if (user.password !== password) {
      throw "Unknown user or password.";
    }

    this.#currentUsername = user.username;
  }

  logout() {
    this.#currentUsername = undefined;
    this.#currentWalletname = undefined;
  }

  set currentWalletname(walletname) {
    this.#currentWalletname = walletname;
  }
}

/**
 * A single transaction.
 */
class Transaction {
  
  #username
  #amount
  #description
  #datetime
  
  constructor(username, amount, description, datetime) {
    if (!username) {
      throw Error("Unknown user.");
    }
    this.#username = username;
    
    if (!amount) {
      throw Error("No amount given.");
    }
    if (typeof amount !== "number") {
      throw Error("Amount is not a number.");
    }
    this.#amount = amount;
    
    this.#description = description;
    
    if (!datetime) {
      datetime = new Date();
    }
    this.#datetime = datetime;
  }
  
  get amount() {
    return this.#amount;
  }
  
  get datetime() {
    return this.#datetime;
  }
  
  get description() {
    return this.#description;
  }
  
  get username() {
    return this.#username;
  }
}

/**
 * A single LunchMonkey user.
 */
class User {

  #username
  #email
  #password
  #avatar
  #recentWalletname

  constructor(username, email, password, avatar) {

    if (!this.checkUsername(username)) {
      throw "Invalid username";
    }

    if (!this.checkEmail(email)) {
      throw "Invalid email";
    }

    if (!this.checkPassword(password)) {
      throw "Invalid password";
    }

    this.#username = username;
    this.#email = email;
    this.#password = password;
    this.#avatar = User.createAvatarURL(this.#email);
    this.#recentWalletname = undefined;
  }

  checkEmail(email) {
    if (!email || typeof email !== "string") {
      return false;
    }

    // This is a regular expression for verifying emails.
    // See: https://www.abstractapi.com/tools/email-regex-guide
    const regex = /^[a-zA-Z0-9.!#$%&’*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/
    return email.match(regex);
  }

  checkPassword(password) {
    if (!password || typeof password !== "string") {
      return false;
    }
    return password.length >= 6;
  }

  checkUsername(username) {
    return !(!username || typeof username !== "string");
  }

  static createAvatarURL(email) {
    if (!email) {
      return LUNCHMONKEY_DEFAULT_AVATAR;
    }
    const emailMD5 = md5(email);
    return `https://www.gravatar.com/avatar/${emailMD5}?d=robohash&size=192`
  }

  get avatar() {
    return this.#avatar;
  }

  get email() {
    return this.#email;
  }

  get password() {
    return this.#password;
  }

  get recentWalletname() {
    return this.#recentWalletname;
  }

  get username() {
    return this.#username;
  }

  set email(email) {
    if (!this.checkEmail(email)) {
      throw "New email of user is not a valid string."
    }
    this.#email = email;
  }

  set recentWalletname(walletname) {
    this.#recentWalletname = walletname;
  }

  set username(username) {
    if (!this.checkUsername(username)) {
      throw "Invalid username.";
    }
    this.#username = username;
  }
}

/**
 * A single wallet.
 */
class Wallet {

  #name
  #adminname
  #usernames
  #transactions
  #disableUsernames

  constructor(name, adminname) {

    if (!this.checkName(name)) {
      throw "Invalid wallet name.";
    }

    if (!this.checkAdmin(adminname)) {
      throw "Invalid admin name.";
    }

    this.#name = name;
    this.#adminname = adminname;
    this.#usernames = [adminname];
    this.#transactions = [];
    this.#disableUsernames = [];
  }
  
  addTransaction(transaction) {
    if (!transaction) {
      throw new Error("No transaction provided.");
    }
    this.#transactions.push(transaction);
  }

  addUser(username) {
    if (username && !this.#usernames.includes(username)) {
      this.#usernames.push(username);
    }
  }

  balance() {
    return this.#transactions.reduce((acc, transaction) => acc + transaction.amount, 0.0);
  }
  
  checkAdmin(adminname) {
    if (!adminname || typeof adminname !== "string") {
      return false;
    }
    return !!LunchMonkey.getUserByName(adminname);
  }

  checkName(name) {
    if (!name || typeof name !== "string") {
      return false;
    }
    return name.length >= 4;
  }
  
  disableUser(username, actingUserName) {
    
    if (actingUserName !== this.#adminname) {
      console.error("Disable user in wallet: acting user is not admin of wallet.");
      return;
    }
    
    if (!username) {
      console.error("Disable user in wallet: no user.");
      return;
    }
    
    if (!this.hasUser(username)) {
      console.warn(`Disable user in wallet: user '${username}' is not participant of wallet.`);
      return;
    }
    
    if (this.#adminname === username) {
      console.warn(`Disable user in wallet: user '${username}' is admin of wallet, cannot remove.`);
      return;
    }
    
    if (this.#disableUsernames.includes(username)) {
      console.warn(`Disable user in wallet: user '${username}' is already not able to participate.`);
      return;
    }
    
    this.#disableUsernames.push(username);
  }
  
  get adminname() {
    return this.#adminname;
  }
  
  get disabledUsernames() {
    return this.#disableUsernames;
  }

  get name() {
    return this.#name;
  }

  get usernames() {
    return this.#usernames;
  }

  hasUser(username) {
    return username ? this.#usernames.includes(username) : false;
  }
  
  set name(name) {
    if (!this.checkName(name)) {
      throw "Invalid wallet name.";
    }
    this.#name = name;
  }
  
  sumPerUser() {
    
    const sums = new Map();
    for (let i in this.#usernames) {
      sums.set(this.#usernames[i], 0.0);
    }

    for (let index in this.#transactions) {
      
      if (this.#transactions[index].amount < 0) {
        continue;
      }
      
      const username = this.#transactions[index].username;
      const amount = this.#transactions[index].amount;
      sums.set(username, amount + sums.get(username));
    }
    return sums;
  }
}

// The one and only (singleton) LunchMonkey app.
const LunchMonkey = new Application();
