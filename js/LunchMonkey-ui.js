"use strict";

/*
 * LunchMonkey-ui.js
 *
 * JavaScript sources making UI/UX.
 */

const UI = {

  dashboardPage: {
    banner: document.querySelector("#pageDashboard .banner"),
    newWalletButton: document.getElementById("newWalletButton"),
    recentWallet: document.querySelector("#pageDashboard .recent-wallet"),
    walletList: document.querySelector("#pageDashboard .wallet-list"),
    logoutButton: document.getElementById("logoutButton")
  },

  loginPage: {
    loginForm: document.getElementById("loginForm"),
    signInText: document.getElementById("signInText"),
    signInPasswordText: document.getElementById("signInPasswordText"),
    signInButton: document.getElementById("signInButton"),
    toRegisterPageButton: document.getElementById("toRegisterPageButton")
  },

  registerPage: {
    registerForm: document.getElementById("registerForm"),
    registerUsernameText: document.getElementById("registerUsernameText"),
    registerEmailText: document.getElementById("registerEmailText"),
    registerPasswordText: document.getElementById("registerPasswordText"),
    registerPasswordRepeatText: document.getElementById("registerPasswordRepeatText"),
    registerButton: document.getElementById("registerButton"),
    backButton: document.querySelector("#pageRegister .backButton")
  },

  walletPage: {
    banner: document.querySelector("#pageWallet .banner"),
    manageWalletButton: document.getElementById("manageWalletButton"),
    backButton: document.querySelector("#pageWallet .backButton"),
    walletName: document.querySelector("#pageWallet .walletBalance .walletName"),
    walletAvatarLine: document.querySelector("#pageWallet .walletAvatarLine"),
    walletBalance: document.querySelector("#pageWallet .walletBalance .walletValue"),
    avatarHoverNameOnWalletPage: document.getElementById("avatarHoverNameOnWalletPage"),
    deposit5Button: document.getElementById("deposit5Button"),
    deposit10Button: document.getElementById("deposit10Button"),
    deposit20Button: document.getElementById("deposit20Button"),
    depositForm: document.getElementById("depositForm"),
    depositAmountText: document.getElementById("depositAmountText"),
    depositButton: document.getElementById("depositButton"),
    withdrawForm: document.getElementById("withdrawForm"),
    withdrawAmountText: document.getElementById("withdrawAmountText"),
    withdrawReasonText: document.getElementById("withdrawReasonText"),
    withdrawButton: document.getElementById("withdrawButton")
  },
  
  manageWalletPage: {
    banner: document.querySelector("#pageWalletManagement .banner"),
    dissolveWalletButton: document.getElementById("dissolveWalletButton"),
    backButton: document.querySelector("#pageWalletManagement .backButton"),
    walletName: document.querySelector("#pageWalletManagement .walletBalance .walletName"),
    walletBalance: document.querySelector("#pageWalletManagement .walletBalance .walletValue"),
    addMemberButton: document.getElementById("addMemberButton"),
    memberList: document.getElementById("memberList"),
    walletChartCanvas: document.getElementById("walletChartCanvas")
  },

  infoDialog: {
    dialog: document.getElementById("infoDialog"),
    infoMessage: document.getElementById("infoMessage"),
    infoDialogOkButton: document.getElementById("infoDialogOkButton")
  },

  errorDialog: {
    dialog: document.getElementById("errorDialog"),
    errorMessage: document.getElementById("errorMessage"),
    errorDialogOkButton: document.getElementById("errorDialogOkButton")
  },

  newWalletDialog: {
    dialog: document.getElementById("newWalletDialog"),
    newWalletForm: document.getElementById("newWalletForm"),
    newWalletText: document.getElementById("newWalletText"),
    newWalletDialogCancelButton: document.getElementById("newWalletDialogCancelButton"),
    newWalletDialogOkButton: document.getElementById("newWalletDialogOkButton")
  },
  
  disableUserInWalletDialog: {
    dialog: document.getElementById("disableUserInWalletDialog"),
    disableUserInWalletMessage: document.getElementById("disableUserInWalletMessage"),
    disableUserInWalletDialogOkButton: document.getElementById("disableUserInWalletDialogOkButton"),
    disableUserInWalletDialogCancelButton: document.getElementById("disableUserInWalletDialogCancelButton")
  },

  pages: {
    pageDashboard: document.getElementById("pageDashboard"),
    pageLogin: document.getElementById("pageLogin"),
    pageRegister: document.getElementById("pageRegister"),
    pageWallet: document.getElementById("pageWallet"),
    pageWalletManagement: document.getElementById("pageWalletManagement")
  },

  // A map of pages and "onvisible" event handlers. Will be setup later.
  onvisible: undefined,

  pagesStack: [],

  avatarImageURL: "assets/icons/user-solid.svg"
}

function checkAmountInput(element) {
  
  if (element) {
    
    const regex = /^\+?\d+(,\d{0,2})?$/;
    const valid = regex.test(element.value);
    const button = document.getElementById(element.getAttribute("button"));
    
    if (button) {
      if (!valid) {
        button.setAttribute("disabled", "");
      } else {
        button.removeAttribute("disabled");
      }
    }
  }
}

function clickedAvatarOnWalletPage(username, element) {
}

function clickedBackButtonOnRegisterPage() {
  popPage();
}

function clickedErrorDialogOkButton() {
  UI.errorDialog.dialog.style.display = "none";
}

function clickedDepositButton() {
  const amount = parseFloat(UI.walletPage.depositAmountText.value);
  deposit(amount);
  UI.walletPage.depositAmountText.value = "";
  checkAmountInput(UI.walletPage.depositAmountText);
}

function clickedDeposit5Button() {
  deposit(5.0);
}

function clickedDeposit10Button() {
  deposit(10.0);
}

function clickedDeposit20Button() {
  deposit(20.0);
}

function clickedDisableUserInWalletDialogCancelButton() {
  UI.disableUserInWalletDialog.dialog.style.display = "none";
}

function clickedDisableUserInWalletDialogOkButton(walletName, username) {
  
  const wallet = LunchMonkey.getWallet(walletName);
  if (!wallet) {
    console.error(`Disable user in wallet: cannot find wallet "${walletName}".`);
    return;
  }
  
  UI.disableUserInWalletDialog.dialog.style.display = "none";
  wallet.disableUser(username, LunchMonkey.currentUsername);
  refresh();
}

function clickedDissolveWalletButton() {
  console.error("clickedDissolveWalletButton");
}

function clickedInfoDialogOkButton() {
  UI.infoDialog.dialog.style.display = "none";
}

function clickedLogoutButton() {
  LunchMonkey.logout();
  resetPageStack();
  pushPage(UI.pages.pageLogin);
}

function clickedManageWalletButton() {
  pushPage(UI.pages.pageWalletManagement);
}

function clickedManageWalletPageBackButton() {
  popPage();
}

function clickedNewWalletDialogCancelButton() {
  UI.newWalletDialog.dialog.style.display = "none";
}

function clickedNewWalletButton() {
  UI.newWalletDialog.dialog.style.display = "block";
  UI.newWalletDialog.newWalletText.value = "";
  UI.newWalletDialog.newWalletText.focus();
}

function clickedNewWalletDialogOkButton() {

  UI.newWalletDialog.dialog.style.display = "none";
  const newWalletName = UI.newWalletDialog.newWalletText.value;
  if (newWalletName.length > 0) {

    try {

      const wallet = new Wallet(newWalletName, LunchMonkey.currentUsername);
      LunchMonkey.addWallet(wallet);
      const user = LunchMonkey.getUserByName(LunchMonkey.currentUsername);
      user.recentWallet = newWalletName;

      onDashboardPageVisible();
      showInfo(`Created new wallet "${wallet.name}".`);

    } catch (ex) {
      showError(ex);
    }
  }
}

function clickedRegisterButton() {

  if (UI.registerPage.registerPasswordText.value !== UI.registerPage.registerPasswordRepeatText.value) {
    showError("Password and repeated password are different.");
    return;
  }

  try {

    const user = new User(
        UI.registerPage.registerUsernameText.value,
        UI.registerPage.registerEmailText.value,
        UI.registerPage.registerPasswordText.value);
    LunchMonkey.addUser(user);

    showInfo(`New user "${user.username}" added. Please log in.`)
    popPage();

  } catch (ex) {
    showError(ex);
  }
}

function clickedSignInButton() {

  resetPageStack();

  try {

    // TODO: remove this later.
    console.warn("Logging in as alice");
    LunchMonkey.login("alice", "123456");

    pushPage(UI.pages.pageDashboard);

  } catch (ex) {
    showError(ex);
  }
}

function clickedToRegisterPageButton() {
  pushPage(UI.pages.pageRegister);
}

function clickedWalletButton(walletname) {

  const wallet = LunchMonkey.getWallet(walletname);
  if (!wallet) {
    showError("Invalid wallet picked.")
    return;
  }

  LunchMonkey.currentWalletname = walletname;
  const user = LunchMonkey.getUserByName(LunchMonkey.currentUsername);
  user.recentWalletname = walletname;
  pushPage(UI.pages.pageWallet);
}

function clickedWalletPageBackButton() {
  popPage();
  LunchMonkey.currentWalletname = undefined;
}

function clickedWithdrawButton() {
  
  const reason = UI.walletPage.withdrawReasonText.value;
  const amount = parseFloat(UI.walletPage.withdrawAmountText.value);
  const username = LunchMonkey.currentUsername;
  const wallet = LunchMonkey.getWallet(LunchMonkey.currentWalletname);
  
  const currentBalance = wallet.balance();
  if (currentBalance - amount < 0.0) {
    showError("Cannot withdraw more than what is in the wallet.");
    return;
  }
  
  if (username && wallet) {
    wallet.addTransaction(new Transaction(username, -amount, reason, new Date()));
  }
  
  UI.walletPage.walletBalance.children[0].textContent = "€ "+ wallet.balance().toFixed(2);
  
  UI.walletPage.withdrawAmountText.value = "";
  UI.walletPage.withdrawReasonText.value = "";
  checkAmountInput(UI.walletPage.withdrawAmountText);
  
  showInfo(`€ ${amount.toFixed(2)} für ${reason} ausgegeben.`);
}

function createDepositAnimation(amount) {
  
  const animatedAmount = UI.walletPage.walletBalance.appendChild(document.createElement("div"));
  animatedAmount.classList.add("walletDepositAmount");
  animatedAmount.textContent = amount;
  
  anime.timeline({
      easing: 'easeOutExpo',
      direction: 'alternate',
      loop: 1,
      complete: (() =>
          UI.walletPage.walletBalance.removeChild(animatedAmount)
      )
    })
    .add({
      targets: animatedAmount,
      duration: 500,
      translateX: [0, -100 + anime.random(0, 100)],
      translateY: [100, -50 + anime.random(0, 50)],
      easing: "easeOutExpo",
      scale: 3,
      color: "rgb(255, 255, 255)",
      opacity: 1
    });
}

function createWalletChart(wallet) {
  
  if (!wallet) {
    throw new Error("Wallet is undefined/null. Cannot create Chart without wallet.");
  }
  
  const sumPerUser = wallet.sumPerUser();
  let average = 0.0;
  for (const amount of sumPerUser.values()) {
    average += amount;
  }
  average /= wallet.usernames.length;
  
  let balancePerUser = []
  for (let entry of sumPerUser.entries()) {
    balancePerUser.push({ user: entry[0], balance: entry[1] - average });
  }
  
  new Chart(
    UI.manageWalletPage.walletChartCanvas,
    {
      type: 'bar',
      data: {
        labels: balancePerUser.map(row => row.user),
        datasets: [
          {
            label: 'Balance os user',
            data: balancePerUser.map(row => row.balance)
          }
        ]
      }
    }
  );
  
}

function deposit(amount) {
  const username = LunchMonkey.currentUsername;
  const wallet = LunchMonkey.getWallet(LunchMonkey.currentWalletname);
  if (username && wallet) {
    wallet.addTransaction(new Transaction(username, amount, undefined, new Date()));
  }
  UI.walletPage.walletBalance.children[0].innerText = "€ "+ wallet.balance().toFixed(2);
  createDepositAnimation(`+ € ${amount.toFixed(2)}`);
}

function disableUserInWallet(walletName, username) {
  
  const wallet = LunchMonkey.getWallet(walletName);
  const user = LunchMonkey.getUserByName(username);
  if (!wallet || !user) {
    console.warn(`Disable user in wallet, with wallet ${walletName} and user ${username} failed to acquire wallet or user.`);
    return;
  }
  
  UI.disableUserInWalletDialog.disableUserInWalletMessage.textContent = `Disable user "${username}" in wallet "${walletName}"?`;
  UI.disableUserInWalletDialog.disableUserInWalletDialogOkButton.onclick = (
    () => clickedDisableUserInWalletDialogOkButton(walletName, username)
  );
  
  UI.disableUserInWalletDialog.dialog.style.display = "block";
  UI.disableUserInWalletDialog.disableUserInWalletDialogOkButton.focus();
}

function mouseleaveAvatarOnWalletPage(username, element) {
  if (UI.walletPage.avatarHoverNameOnWalletPage) {
    UI.walletPage.avatarHoverNameOnWalletPage.innerText = "";
  }
}

function mousemoveAvatarOnWalletPage(username, element) {
  if (username && UI.walletPage.avatarHoverNameOnWalletPage) {
    const user = LunchMonkey.getUserByName(username);
    if (user) {
      UI.walletPage.avatarHoverNameOnWalletPage.innerText = `${user.username} (${user.email})`;
    }
  }
}

function onDashboardPageVisible() {
  updateBanner(UI.dashboardPage.banner);
  updateRecentWallet(UI.dashboardPage.recentWallet);
  updateWalletList(UI.dashboardPage.walletList);
  UI.dashboardPage.recentWallet.focus();
}

function onLoginPageVisible() {
  UI.loginPage.signInText.focus();
}

function onRegisterPageVisible() {
  UI.registerPage.registerUsernameText.focus();
}

function onWalletPageVisible() {
  updateBanner(UI.walletPage.banner);
  const wallet = LunchMonkey.getWallet(LunchMonkey.currentWalletname);
  UI.walletPage.walletName.textContent = wallet.name;
  UI.walletPage.walletBalance.children[0].textContent = "€ "+ wallet.balance().toFixed(2);
  UI.walletPage.walletAvatarLine.innerHTML = walletAvatars(wallet,
    clickedAvatarOnWalletPage,
    mousemoveAvatarOnWalletPage,
    mouseleaveAvatarOnWalletPage);
}

function onWalletManagementPageVisible() {
  
  updateBanner(UI.manageWalletPage.banner);
  const wallet = LunchMonkey.getWallet(LunchMonkey.currentWalletname);
  
  UI.manageWalletPage.walletName.textContent = wallet.name;
  UI.manageWalletPage.walletBalance.textContent = "€ "+ wallet.balance().toFixed(2);
  
  UI.manageWalletPage.addMemberButton.style.display = (wallet.adminname === LunchMonkey.currentUsername ? "block" : "none");
  UI.manageWalletPage.memberList.innerHTML = memberList(wallet, LunchMonkey.currentUsername);
  
  createWalletChart(wallet);
}

function popPage() {
  if (UI.pagesStack.length === 0) {
    console.error("popPage called on empty page stack.");
    return;
  }
  UI.pagesStack.pop();
  showPage(UI.pagesStack[UI.pagesStack.length - 1]);
}

function pushPage(page) {
  if (!page) {
    console.error("pushPage called without valid page.");
    return;
  }
  UI.pagesStack.push(page);
  showPage(page);
}

function refresh() {
  const page = UI.pagesStack[UI.pagesStack.length - 1];
  showPage(page);
}

function resetPageStack() {
  UI.pagesStack = [];
}

function showError(message) {
  UI.errorDialog.errorMessage.textContent = message;
  UI.errorDialog.dialog.style.display = "block";
  UI.errorDialog.errorDialogOkButton.focus();
}

function showInfo(message) {
  UI.infoDialog.infoMessage.textContent = message;
  UI.infoDialog.dialog.style.display = "block";
  UI.infoDialog.infoDialogOkButton.focus();
}

function showPage(page) {

  for (let p in UI.pages) {

    if (UI.pages[p] === page) {

      UI.pages[p].style.display = "block";

      const onvisible = UI.onvisible.get(UI.pages[p], undefined);
      if (onvisible) {
        onvisible();
      }

    } else {
      UI.pages[p].style.display = "none";
    }
  }
}

function updateBanner(bannerElement) {
  if (!bannerElement) {
    return;
  }
  const user = LunchMonkey.getUserByName(LunchMonkey.currentUsername);
  bannerElement.innerHTML = banner(user);
}

function updateRecentWallet(recentWallet) {
  if (!recentWallet) {
    return;
  }
  const button = recentWalletButton(LunchMonkey.getUserByName(LunchMonkey.currentUsername), "btn-primary");
  recentWallet.innerHTML = `
    <div class="h1">Recent</div>
    ${button}
  `;
}

function updateWalletList(walletListElement) {
  if (!walletListElement) {
    return;
  }
  const user = LunchMonkey.getUserByName(LunchMonkey.currentUsername);
  walletListElement.innerHTML = walletList(user);
}

// This wires up all UI elements at start.
(() => {

  UI.loginPage.loginForm.addEventListener("submit", (e) => {
    e.preventDefault();
    clickedSignInButton();
  });
  UI.loginPage.signInButton.onclick = clickedSignInButton;
  UI.loginPage.toRegisterPageButton.onclick = clickedToRegisterPageButton;

  UI.registerPage.registerForm.addEventListener("submit", (e) => {
    e.preventDefault();
    clickedRegisterButton();
  });
  UI.registerPage.registerButton.onclick = clickedRegisterButton;
  UI.registerPage.backButton.onclick = clickedBackButtonOnRegisterPage;

  UI.dashboardPage.newWalletButton.onclick = clickedNewWalletButton;
  UI.dashboardPage.logoutButton.onclick = clickedLogoutButton;

  UI.walletPage.manageWalletButton.onclick = clickedManageWalletButton;
  UI.walletPage.backButton.onclick = clickedWalletPageBackButton;
  UI.walletPage.deposit5Button.onclick = clickedDeposit5Button;
  UI.walletPage.deposit10Button.onclick = clickedDeposit10Button;
  UI.walletPage.deposit20Button.onclick = clickedDeposit20Button;
  UI.walletPage.depositForm.addEventListener("submit", (e) => {
    e.preventDefault();
  });
  UI.walletPage.depositButton.onclick = clickedDepositButton;
  UI.walletPage.withdrawForm.addEventListener("submit", (e) => {
    e.preventDefault();
  });
  UI.walletPage.withdrawButton.onclick = clickedWithdrawButton;
  
  UI.manageWalletPage.backButton.onclick = clickedManageWalletPageBackButton;
  UI.manageWalletPage.dissolveWalletButton.onclick = clickedDissolveWalletButton;

  UI.infoDialog.infoDialogOkButton.onclick = clickedInfoDialogOkButton;
  UI.infoDialog.infoDialogOkButton.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      clickedInfoDialogOkButton();
    }
  });
  UI.infoDialog.infoMessage.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      clickedInfoDialogOkButton();
    }
  });

  UI.errorDialog.errorDialogOkButton.onclick = clickedErrorDialogOkButton;
  UI.errorDialog.errorDialogOkButton.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      clickedErrorDialogOkButton();
    }
  });
  UI.errorDialog.errorMessage.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      clickedErrorDialogOkButton();
    }
  });

  UI.newWalletDialog.newWalletForm.addEventListener("submit", (e) => {
    e.preventDefault();
    clickedNewWalletDialogOkButton();
  });
  UI.newWalletDialog.newWalletForm.addEventListener("keyup", (e) => {
    if (e.key === "Escape") {
      clickedNewWalletDialogCancelButton();
    }
  });
  UI.newWalletDialog.newWalletDialogOkButton.onclick = clickedNewWalletDialogOkButton;
  UI.newWalletDialog.newWalletDialogCancelButton.onclick = clickedNewWalletDialogCancelButton;
  
  UI.disableUserInWalletDialog.disableUserInWalletDialogCancelButton.onclick = clickedDisableUserInWalletDialogCancelButton;

  UI.onvisible = new Map();
  UI.onvisible.set(UI.pages.pageDashboard, onDashboardPageVisible);
  UI.onvisible.set(UI.pages.pageLogin, onLoginPageVisible);
  UI.onvisible.set(UI.pages.pageRegister, onRegisterPageVisible);
  UI.onvisible.set(UI.pages.pageWallet, onWalletPageVisible);
  UI.onvisible.set(UI.pages.pageWalletManagement, onWalletManagementPageVisible);

})();

// Start!
resetPageStack();
pushPage(UI.pages.pageLogin);

console.debug("LunchMonkey-ui.js loaded.");
