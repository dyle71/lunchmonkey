"use strict";

/*
 * LunchMonkey-seed.js
 *
 * Seed in some data into LunchMonkey to already have some items ready.
 */

// Add some users.

LunchMonkey.addUser(new User("alice", "alice@example.com", "123456"));
LunchMonkey.addUser(new User("bob", "bob@example.com", "123456"));
LunchMonkey.addUser(new User("charly", "charly@example.com", "123456"));
LunchMonkey.addUser(new User("denise", "denise@example.com", "123456"));

// Add some wallets.

const fastFooderWallet = new Wallet("Fast-Fooder", "alice");
fastFooderWallet.addUser("bob");
fastFooderWallet.addUser("charly");
LunchMonkey.addWallet(fastFooderWallet);

fastFooderWallet.addTransaction(new Transaction("alice", 10.0, undefined, new Date(2023, 4, 17, 9, 5)));
fastFooderWallet.addTransaction(new Transaction("alice", 10.0, undefined, new Date(2023, 4, 17, 9, 10)));
fastFooderWallet.addTransaction(new Transaction("bob", 5.0, undefined, new Date(2023, 4, 17, 9, 30)));
fastFooderWallet.addTransaction(new Transaction("charly", 15.0, undefined, new Date(2023, 4, 17, 10, 45)));
fastFooderWallet.addTransaction(new Transaction("alice", -25.0, "Pizza", new Date(2023, 4, 17, 12, 0)));

const funStuffWallet = new Wallet("Fun Stuff", "bob");
funStuffWallet.addUser("alice");
funStuffWallet.addUser("denise");
LunchMonkey.addWallet(funStuffWallet);

const nekaCheckaWallet = new Wallet("Neka-Checka", "charly");
nekaCheckaWallet.addUser("alice");
nekaCheckaWallet.addUser("denise");
LunchMonkey.addWallet(nekaCheckaWallet);

const schnitzelKriegWallet = new Wallet("Schnitzelkrieg!", "denise");
LunchMonkey.addWallet(schnitzelKriegWallet);
