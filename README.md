# LunchMonkey

A tool for managing a group wallet to deposit and withdraw money for a
group of people on lunch.

## Building

This project requires bootstrap and sass locally installed.

### Bootstrap

Download the latest Boostrap 5.3 from [Get Bootstrap](https://getbootstrap.com/docs/5.3/getting-started/download/),
extract it and place a "bootstrap-5" link to the extracted folder. 

### Anime.js

Download the latest anime.js from (https://github.com/juliangarnier/anime/),
extract it and place an "anime" link to the extracted folder.


### Chart.js

Download the latest Chart.js from (https://github.com/chartjs/Chart.js/releases/tag/),
extract it and place a "chart.js" link to the extracted folder.

### SaSS

Install SaSS for enable the LunchMonkey styles to Boostrap. 

Then run 
```
cd css
sass LunchMonkey-Bootstrap.scss LunchMonkey-Bootstrap.css
```
to create the LunchMonkey bootstrap css.

---  

Oliver Maurhart  
oliver.maurhart@htl-villach.at
